#![warn(future_incompatible, rust_2018_compatibility, rust_2018_idioms, unused)]
#![warn(clippy::pedantic)]
// #![warn(clippy::cargo)]
#![cfg_attr(feature = "strict", deny(warnings))]

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_ne!(2 + 2, 3);
    }
}
